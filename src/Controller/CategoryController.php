<?php


namespace App\Controller;


use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    private CategoryRepository $categoryRepository;
    private ItemRepository $itemRepository;

    public function __construct(CategoryRepository $categoryRepository, ItemRepository $itemRepository)
    {
        $this->categoryRepository = $categoryRepository;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @Route("/category", methods={"POST"}, name="createCategory")
     */
    public function createCategory(Request $request)
    {
        $payload = json_decode((string) $request->getContent(), true);
        if (!array_key_exists("name", $payload)) {
            throw new BadRequestHttpException("name attribute must be defined");
        }
        $category = new Category();
        $category->setName($payload["name"]);

        /** @var Category $parentCategory */
        $parentCategory = null;
        if (array_key_exists("parentId", $payload)) {
            $parentCategory = $this->categoryRepository->find($payload["parentId"]);
            if ($parentCategory == null) {
                throw new BadRequestHttpException("parent category does not exist");
            } else {
                $category->setParent($parentCategory);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();

        return new Response('', Response::HTTP_CREATED, [
            'Location' => $this->generateUrl('createCategory', ['category' => $category->getId()]),
            'X-Category-Id' => $category->getId(),
        ]);
    }
}