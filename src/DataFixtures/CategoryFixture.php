<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $parrot = new Category();
        $parrot->setName("parrot");
        $manager->persist($parrot);

        $lorikeet = new Category();
        $lorikeet->setName("lorikeet");
        $lorikeet->setParent($parrot);
        $manager->persist($lorikeet);

        $manager->flush();
    }
}
