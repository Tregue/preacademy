<?php


namespace App\Tests\Controller;


use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CategoryControllerTest extends WebTestCase
{
    private CategoryRepository $categoryRepository;

    protected function setUp()
    {
        parent::setUp();
        $this->categoryRepository = $this->createMock(CategoryRepository::class);
    }

    public function testCategoryCreate()
    {
        $client = self::createClient();
        $client->request(
            'POST',
            '/category',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"name":"avion"}'
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        /** @var Category $created */
        $created = $this->categoryRepository->find($response->headers->get('X-Transaction-Id'));
        $this->assertNotNull($created);
        $this->assertEquals("avion", $created->getName());
    }

    public function testCategoryCreateWithParent()
    {
        $client = self::createClient();
        $parrotCategoryId = $this->categoryRepository->findOneBy(["name"=>"parrot"])->getId();
        $client->request(
            'POST',
            '/submit',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            "{'category':'cockatiel', 'parent'=$parrotCategoryId}"
        );
        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        /** @var Category $created */
        $created = $this->categoryRepository->find($response->headers->get('X-Transaction-Id'));
        $this->assertNotNull($created);
        $this->assertEquals("cockatiel", $created->getName());
        $this->assertEquals("parrot", $created->getParent()->getName());
    }
}